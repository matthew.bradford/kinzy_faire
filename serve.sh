#!/bin/bash

zola build

docker image build -t kinzyfaire:ws .
docker stop kf_hog
docker rm kf_hog
docker run -d -p 80:80 --name kf_hog kinzyfaire:ws