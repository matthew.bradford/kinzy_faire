+++
title = "Kinzy Faire Habanero Sauce"
date = 2019-10-10 
+++

Happy Fall!


It is autumn and my garden did not do as well this year as last. I had planted tomato and hot peppers. Our mild wet summer prevented most of my harvest from getting ripe! But I still got enough for a couple of batches (about 3 gallons) of tomato sauce and one batch of habanero sauce.


Habanero Sauce


Any recipe is going to be very variable depending on how ripe your fruit; take this with 2 grains of salt and a shot of tequila and keep the tequila around if your goofy enough to do this without wearing rubber gloves.



Ingredients:  
    • 8 lbs of tomato    
    • 8 lbs of sweet peppers    
    • 2 lbs random mild/hot (jalapeno) peppers    
    • 20-30 habanero peppers    
    • 2 large white onions    
    • A lot of garlic cloves    
    • 2 Tblsp Salt    
    • 1 1/2 cups vinegar    
       
For the sweet flavor Italian red roasting pepper, lipstick red pepper, and red chili pepper are the best, but if those are hard to find any sweet pepper will work. I like red peppers so my sauce has a nice red color. If you want your habanero sauce to be more orange throw in some yellow peppers!

Set the oven to broil (you will be at this a while)

Cut everything in half and remove the soft white flesh inside the peppers and the majority of the seeds. I do the same for the tomatoes. As you are doing that broil the result until the skin chars. Start with the tomato because they take the longest to cook and keep the peppers from burning on the bottom of your pot. I leave skins on both tomato and peppers, but your sauce will be smoother without. Skins on both are easy to remove after broiling.

Leave the habanero seeds and broil them and do the garlic/onions.

Scrape the charred halves into a large pot (this makes about 1 - 1 1/2 gallons of sauce) and start reducing on medium heat. All the water will come out of the fruit and everything will start to soften.

Once the contents of the pot are soft (about 30 minutes) use a stick blender to turn everything into a nice paste.

Continue to reduce everything until you reach your desired consistency (keep in mind it is a little thicker once cool).

Add vinegar and salt to taste. If your peppers/tomato's are not as sweet as you would like add little maple syrup or sugar.


